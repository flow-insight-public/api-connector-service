/**
 * Copyright (c) 2022. Knowledge Insight
 *
 * Import Application
 * 
 * Opmerking: Dit programma werkt alleen als je verbindt met de sandbox omgeving van Knowledge Insight
 *
 * @author Bert van Deuveren
 */
import dotenv from 'dotenv';
dotenv.config();

import fs from 'fs'
import path from 'path'
import Excel from 'exceljs'
import axios from 'axios';
import async from 'async';
import crypto from 'crypto'
import { v4 as uuid } from 'uuid';

const API_BASE_URL = process.env.API_BASE_PATH ?? 'https://sandbox.knowledge-insight.com'
const API_BASE_PATH = process.env.API_BASE_URL ?? `${API_BASE_URL}/api/v1`;
const TEAM_ID = process.env.TEAM_ID ?? '<<YOUR TEAM ID>>'
const CLIENT_ID = process.env.CLIENT_ID ?? '<<YOUR CLIENT ID>>';
const CLIENT_SECRET = process.env.CLIENT_SECRET ?? '<<YOUR SECRET>>';

const Logger = console;

export type Uuid = string;
export const toUuid = (): Uuid => uuid();

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
// API Routines
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
const retrieveToken = async (): Promise<string> => {
  const response = await axios.request({
    url: "/api/oauth2/token",
    method: "post",
    baseURL: `${API_BASE_URL}`,
    auth: {
      username: `${CLIENT_ID}`,
      password: `${CLIENT_SECRET}`
    },
    data: {
      grant_type: "client_credentials",
      scope: "api",
    }
  });
  return response.data.access_token;
}

const httpOptions = {
  headers: {
    'Content-Type': 'application/json',
    'Authorization': ''
  }
}

const makeWorkorder = (name: string, description: string, startAt: Date, dueDate: Date, location: string, accountId: Uuid, finished: (err?: any) => void) => {
  const workOrder = {
    id: CLIENT_ID + crypto.createHmac('sha256', name).digest('hex'),
    name: name,
    description: description,
    accountKeys: [accountId],
    startAt: startAt,
    endBefore: dueDate,
    tags: [location].concat(location.split(' '))
  }
  axios.delete(`${API_BASE_PATH}/workorders/${workOrder.id}`, httpOptions)
    .catch((err) => {
      Logger.warn(err.response.data.message.results)
    })
    .finally(() => {
      axios.post(`${API_BASE_PATH}/workorders`, workOrder, httpOptions)
        .then((result) => {
          finished()
        })
        .catch((err: any) => {
          finished(err)
        })
    })
}

const makeLocation = (name: string, description: string, identifier: string, finished: (err?: any) => void) => {
  const location = {
    id: CLIENT_ID + crypto.createHmac('sha256', name).digest('hex'),
    locationType: 'AREACODE',
    name: name,
    locationIdenifier: identifier,
    description: description
  }
  axios.delete(`${API_BASE_PATH}/locations/${location.id}`, httpOptions)
    .catch((err) => {
      Logger.warn(err.response.data.message.results)
    })
    .finally(() => {
      axios.post(`${API_BASE_PATH}/locations`, location, httpOptions)
        .then((result) => {
          finished()
        })
        .catch((err: any) => {
          finished(err)
        })
    })
}

const makeWorkflow = (key: string, name: string, description: string, steps: any[], connections: any[], finished: (err?: any) => void) => {
  const wf = {
    id: CLIENT_ID + crypto.createHmac('sha256', key).digest('hex'),
    name: name,
    isArchived: false,
    title: name,
    description: description,
    properties: [],
    steps: steps,
    connections: connections
  }
  axios.delete(`${API_BASE_PATH}/workflows/${wf.id}`, httpOptions)
    .catch((err) => {
      Logger.warn(err.response.data.message.results)
    })
    .finally(() => {
      axios.post(`${API_BASE_PATH}/workflows`, wf, httpOptions)
        .then(() => {
          axios.post(`${API_BASE_PATH}/workflows/${wf.id}/publish`, wf, httpOptions)
            .then(() => {
              finished()
            })
            .catch((err: any) => {
              finished(err)
            })
        })
        .catch((err: any) => {
          finished(err)
        })
    })
}

const makeTask = (key: string, name: string, description: string, externalReference: string, workflowKey: string, workorderKey: string, finished: (err?: any) => void) => {
  const workOrder = {
    id: CLIENT_ID + crypto.createHmac('sha256', key).digest('hex'),
    name: name,
    description: description,
    workflowKey: workflowKey,
    externalReference: externalReference,
    workorderKey: workorderKey
  }
  axios.delete(`${API_BASE_PATH}/tasks/${workOrder.id}`, httpOptions)
    .catch((err) => {
      Logger.warn(err.response.data.message.results)
    })
    .finally(() => {
      axios.post(`${API_BASE_PATH}/tasks`, workOrder, httpOptions)
        .then((result) => {
          finished()
        })
        .catch((err: any) => {
          finished(err)
        })
    })
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Import Locations
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
type TImportLocation = {
  fLocation: string,
  location: string,
  flDescription: string,
  inspectionCharacteristic: string
}

interface TLocationDictionary {
  [key: string]: TImportLocation;
}

const restrieveLocations = (workbook: Excel.Workbook): TLocationDictionary => {
  const worksheet = workbook.getWorksheet(4)
  const ret: TLocationDictionary = {}
  worksheet.eachRow((p: Excel.Row, index: number) => {
    if (index > 1) {
      const key = p.getCell(7).toString();
      ret[key] = {
        fLocation: p.getCell(2).toString(),
        location: p.getCell(7).toString(),
        flDescription: p.getCell(3).toString(),
        inspectionCharacteristic: p.getCell(6).toString()
      }
    }
  })

  return ret;
}

const importLocations = (locations: TLocationDictionary, finished: (err: any) => void) => {
  const locationNames = Object.keys(locations)
  async.each(locationNames,
    (p: string, cb: any) => {
      const loc = locations[p]
      makeLocation(loc.location, `${loc.flDescription} (${loc.inspectionCharacteristic})`, loc.fLocation, (err) => {
        cb(err)
      })
    },
    (err: any) => {
      if (err) {
        Logger.error(err)
      }
      Logger.log("Locations added.")
      finished(err)
    })
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Import Workorders
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
type TImportWorkorder = {
  mainPlant: string;
  orderType: string;
  orderNumber: string;
  dueDate: Date;
  description: string;
  startAt: Date;
}

interface TWorkorderDictionary {
  [key: string]: TImportWorkorder;
}

const retrieveWorkorder = (workbook: Excel.Workbook): TWorkorderDictionary => {
  const worksheet = workbook.getWorksheet(1)
  const ret: TWorkorderDictionary = {};

  worksheet.eachRow((p: Excel.Row, index: number) => {
    if (index > 1) {
      const key = p.getCell(3).toString();
      ret[key] = {
        mainPlant: p.getCell(1).toString(),
        orderType: p.getCell(2).toString(),
        orderNumber: p.getCell(3).toString(),
        dueDate: new Date(p.getCell(4).toString()),
        description: p.getCell(7).toString(),
        startAt: new Date(p.getCell(15).toString()),
      }
    }
  });

  return ret;
}
const importWorkorders = (workorders: TWorkorderDictionary, finished: (err: any) => void) => {
  const locationNames = Object.keys(workorders)
  async.each(locationNames,
    (key: string, cb: any) => {
      const wo = workorders[key]
      makeWorkorder(wo.orderNumber, wo.description, wo.startAt, wo.dueDate, wo.mainPlant ?? '', TEAM_ID, (err) => {
        cb(err)
      })
    },
    // eslint-disable-next-line sonarjs/no-identical-functions
    (err: any) => {
      if (err) {
        Logger.error(err.response.data.message.results)
      }
      Logger.log("Workorders added.")
      finished(err)
    })
}


// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Import Tasks
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
type TImportTask = {
  key: string,
  name: string;
  description: string;
  externalReference: string;
  workflowKey: string;
  workorderKey: string;
  tags: string[];
}

interface TTaskDictionary {
  [key: string]: Array<TImportTask>;
}

const restrieveTasks = (workbook: Excel.Workbook): TTaskDictionary => {
  const worksheet = workbook.getWorksheet(2)
  const workflows = retrieveWorkflows(workbook)
  const workflowKeys = Object.keys(workflows)
  const ret: TTaskDictionary = {}
  worksheet.eachRow((p: Excel.Row, index: number) => {
    if (index > 1) {
      const key = `${p.getCell(1).toString()}-${p.getCell(2).toString()}`;
      if (!ret[key]) {
        ret[key] = []
      }
      const ilasWorkflows = workflowKeys.filter(r => r.startsWith(p.getCell(1).toString() + '-' + p.getCell(2).toString()));
      if (ilasWorkflows.length > 0) {
        ilasWorkflows.forEach(r => {
          const workflow = workflows[r]
          ret[key].push({
            key: `${p.getCell(4).toString()}-${workflow[0].location}`,
            name: `${p.getCell(4).toString()} - ${workflow[0].location}`,
            description: p.getCell(5).toString(),
            externalReference: key,
            workflowKey: CLIENT_ID + crypto.createHmac('sha256', workflow[0].key).digest('hex'),
            workorderKey: CLIENT_ID + crypto.createHmac('sha256', p.getCell(1).toString()).digest('hex'),
            tags: [`${workflow[0].location}`]
          })
        })
      } else {
        const step = {
          id: toUuid(),
          tags: [],
          template: 'INFORMATION',
          name: p.getCell(4).toString(),
          title: p.getCell(4).toString(),
          description: p.getCell(4).toString(),
          bodyText: p.getCell(5).toString(),
          control: {
            "minRequiredPreSteps": 1,
            "mandatory": true,
            "tags": []
          },
          choices: [],
          extraInfo: {
            id: toUuid(),
            bodyText: p.getCell(5).toString(),
            title: p.getCell(4).toString(),
            type: "EXTRA_INFO",
            media: []
          }
        }
        const v1 = {
          id: toUuid(),
          fromId: step.id,
          toId: exitStep.id
        }
        const v2 = {
          id: toUuid(),
          fromId: enterStep.id,
          toId: step.id
        }
        const steps = [enterStep, (step as any), exitStep];
        const conn = [v1, v2] as any[]

        makeWorkflow(key, p.getCell(4).toString(), p.getCell(4).toString(), steps, conn, () => {
          Logger.info('Operation workflow added.')
        })
        ret[key].push({
          key: `${p.getCell(4).toString()}`,
          name: `${p.getCell(4).toString()}`,
          description: p.getCell(5).toString(),
          externalReference: key,
          workflowKey: CLIENT_ID + crypto.createHmac('sha256', key).digest('hex'),
          workorderKey: CLIENT_ID + crypto.createHmac('sha256', p.getCell(1).toString()).digest('hex'),
          tags: []
        })
      }
    }
  })

  return ret;
}

const importTasks = (tasks: TTaskDictionary, finished: (err: any) => void) => {
  const locationNames = Object.keys(tasks)
  async.each(locationNames,
    (key: string, cb: any) => {
      const wo = tasks[key]
      async.each(wo,
        (item: TImportTask, innerCb) => {
          makeTask(item.key, item.name, item.externalReference, item.externalReference, item.workflowKey, item.workorderKey, (err) => {
            innerCb(err)
          })
        },
        (err) => {
          cb(err)
        }
      )
    },
    // eslint-disable-next-line sonarjs/no-identical-functions
    (err: any) => {
      if (err) {
        Logger.error(err.response.data.message.results)
      }
      Logger.log("Tasks added.")
      finished(err)
    })
}


// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Import Workflows
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
type TImportWorkflow = {
  ord: number,
  fLocation: string,
  location: string,
  flDescription: string,
  inspectionCharacteristic: string,
  measurementType: string,
  activity: string,
  operationText: string,
  orderNumber: string,
  name: string,
  key: string
}

interface TWorkflowDictionary {
  [key: string]: Array<TImportWorkflow>;
}

const retrieveWorkflows = (workbook: Excel.Workbook): TWorkflowDictionary => {
  const worksheet = workbook.getWorksheet(4)
  const ret: TWorkflowDictionary = {}
  worksheet.eachRow((p: Excel.Row, index: number) => {
    if (index > 1) {
      const key = `${p.getCell(1).toString()}-${p.getCell(4).toString()}-${p.getCell(7).toString()}`;
      if (!ret[key]) {
        ret[key] = []
      }
      ret[key].push({
        ord: index,
        orderNumber: p.getCell(2).toString(),
        fLocation: p.getCell(2).toString(),
        location: p.getCell(7).toString(),
        flDescription: p.getCell(3).toString(),
        inspectionCharacteristic: p.getCell(6).toString(),
        measurementType: p.getCell(10).toString(),
        activity: p.getCell(4).toString(),
        operationText: p.getCell(5).toString(),
        name: `${p.getCell(1).toString()} - ${p.getCell(4).toString()} - ${p.getCell(7).toString()}`,
        key: key
      })
    }
  })

  return ret;
}

const getTemplate = (measurementType: string): string => {
  if (measurementType === 'P_FF_F') {
    return "MULTIPLECHOICE"
  }
  if (measurementType === 'Y_N') {
    return "MULTIPLECHOICE"
  }
  if (measurementType === 'NUMERIC') {
    return "INPUTVALUE"
  }
  return 'INFORMATION'
}

const getChoices = (measurementType: string) => {
  if (measurementType === 'P_FF_F') {
    return [{
      id: toUuid(),
      name: "Pass"
    },
    {
      id: toUuid(),
      name: "Fail & Fixed"
    }, {
      id: toUuid(),
      name: "Fail"
    }]
  }
  if (measurementType === 'Y_N') {
    return [{
      id: toUuid(),
      name: "Yes"
    },
    {
      id: toUuid(),
      name: "No"
    }]
  }
  return []
}

const enterStep = {
  "id": toUuid(),
  "tags": [],
  "template": "ENTER",
  "control": {
    "minRequiredPreSteps": 1,
    "mandatory": true,
    "tags": []
  },
  "choices": [],
  "extraInfo": {
    "id": "",
    "bodyText": "",
    "title": "",
    "type": "EXTRA_INFO"
  }
}
const exitStep = {
  "id": toUuid(),
  "tags": [],
  "template": "EXIT",
  "control": {
    "minRequiredPreSteps": 1,
    "mandatory": true,
    "tags": []
  },
  "choices": [],
  "extraInfo": {
    "id": "",
    "bodyText": "",
    "title": "",
    "type": "EXTRA_INFO"
  }
}

const importWorkflows = (workflowDictionary: TWorkflowDictionary, finished: (err: any) => void) => {
  const workflowNames = Object.keys(workflowDictionary)

  async.each(workflowNames,
    (key: string, cb: any) => {
      const steps = workflowDictionary[key];
      const wfActivities = [enterStep, exitStep] as Array<any>
      const wfConnections = [] as Array<any>

      steps.forEach((s: TImportWorkflow) => {
        const step = {
          id: toUuid(),
          tags: [],
          template: getTemplate(s.measurementType),
          name: s.flDescription,
          title: s.inspectionCharacteristic,
          description: s.inspectionCharacteristic,
          bodyText: `${s.inspectionCharacteristic} - ${s.flDescription}`,
          control: {
            "minRequiredPreSteps": 1,
            "mandatory": true,
            "tags": []
          },
          choices: getChoices(s.measurementType),
          extraInfo: {
            id: "",
            bodyText: "",
            title: "",
            type: "EXTRA_INFO",
            media: []
          }
        }
        const v1 = {
          id: toUuid(),
          fromId: step.id,
          toId: exitStep.id
        }
        const v2 = {
          id: toUuid(),
          fromId: enterStep.id,
          toId: step.id
        }
        wfActivities.push(step as any);
        wfConnections.push(v1 as any);
        wfConnections.push(v2 as any);
      });

      makeWorkflow(key, steps[0].name, steps[0].flDescription, wfActivities, wfConnections, (err) => {
        cb(err)
      })
    },
    (err: any) => {
      if (err) {
        Logger.error(err.response.data.message.results)
      }
      Logger.log("Workflows added.")
      finished(err)
    })
}


// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Read Excel
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
const walk = (currentDirPath: string, callback: (filePath: string, stat: fs.Stats) => void): void => {
  fs.readdir(currentDirPath, (err: any, files: Array<string>) => {
    if (err) {
      throw new Error(err);
    }
    files.filter(p => !p.startsWith('~')).forEach((name: string) => {
      const filePath = path.join(currentDirPath, name);
      const stat = fs.statSync(filePath);
      if (stat.isFile()) {
        callback(filePath, stat);
      } else if (stat.isDirectory()) {
        walk(filePath, callback);
      }
    });
  });
}

export const importExcel = async (): Promise<void> => {
  const startPath = process.env.IMPORT_DIR ?? path.resolve('imports')

  const token: string = await retrieveToken();
  httpOptions.headers.Authorization = `Bearer ${token}`

  walk(startPath, (filePath: string, stat: fs.Stats) => {
    Logger.debug(filePath)

    const workbook = new Excel.Workbook();
    workbook.xlsx.readFile(path.resolve(filePath))
      .then((workbook: Excel.Workbook) => {
        importLocations(restrieveLocations(workbook), () => {
          importWorkorders(retrieveWorkorder(workbook), () => {
            importWorkflows(retrieveWorkflows(workbook), () => {
              importTasks(restrieveTasks(workbook), () => {
                Logger.info('Done.')
              })
            })
          })
        })
      })
      .catch((err: any) => {
        Logger.error(err)
      });
  })
};


// eslint-disable-next-line @typescript-eslint/no-misused-promises
setImmediate(async () => await importExcel())