# VMM ShowCase Api Connector Service

## Installation

#### 1. Clone this repo

```
$ git clone https://gitlab.com/flow-insight-public/api-connector-service.git api-connector-service
$ cd api-connector-service
```

`Important! Change in src/app.ts CLIENT_ID and CLIENT_SECRET to your credentials.`
<br />
<br />
#### 2. Install dependencies

```
$ npm i
```

### 3. Start program

```
$ npm run quick
```
